A Dred End - ALPHA 19 ( WIP ) Experimental version - Not EAC safe


# A Dred End

This 7 Days to Die Mod is a Work In Progress towards a more logical, scientific, deeper, scary and adapting version of the base game.

The goal of this mod is to expand on the base game to allow the player adapt to a changing, dynamic world. 
Through the use of research you will unlock new techniques, items, blocks, powers and knowledge.

NOTE: This is a work in progress. I have a very long list of thing to implement and I am adding them as I have time and knowledge of how to make them. 
At the moment the mod is in an early testing state to see how the systems I have built work so far.
I will be altering this to be an DMT mod eventually. There will also be changes to the progression system, skills and quests all which could break save games. 
I will endevour to only release world or character breaking updates as more major updates so that one knows and will not break their games.

NOTE: A19 is almost done there isnt going to be a lot of time to work on this mod.
As such people have asked about it and so here it is. My focus will be on getting little things done to help with the a20 release. Bug fixes and polish of what there is in the mod now is what is the main focus. 


Additions beyond vanilla:
Research system:
Create a research desk and start crafting experiments. 
    These will get you research notes and the notes will allow you to create schematics for the things you need to build.
    NOTE: I need to tune this feature. the number for each item are very arbitratry and not tuned to fit each schematic. eg engines are expensive to test and could be cheaper to develop
    
    Some recipes are in the advanced research desk - like the molotov on a stick
Planned: Find items in the world are new and will allow you unlock new ideas and schematics that can lead to new abilities, blocks and items. 
 
Do keep in mind that you can still get schematic the old way, by finding or buying them. This system is just to give you more options and to set up a system for future tech.



Reduced gameyness:
There are some things in the game that feel like they are only in there to make certain reward cycles work. 
I aim to reduce this to give the game a greater feeling of immersion.
Pick up the lights in POIS.
Find bicycles in peoples garages.
Experimenting with cooking and see what you can learn


WIP: Destroy the infected cloning facility. 
Only will work in the main map as its the one that has the cloning facility.


New Weapons:
Many Planned
The mod was going to be a melee mod and thus has new weapons and there are many planned in updates.

    Thagomizer a new club based on the tail of a stegosaurs. Should be a one shot on easy zombies with a hard swing.
    
    Spears - a couple of new spears
    
    War pick, War axe and War hammer
    
    Most tools also have a version where there is a handle that can be altered with different tool or weapon mods to make various combinations
    

    Melee signs:
        It should be the case that all sings in the game can be weapons. All you need to do is break them free from there bonds and start smashing :)
    
    Mining laser is creative mod only for now. I have to implement various research trees



Zombie Changes:
There are more zombies

Better eye sight, so get in doors at night

Reduced range so zombies dont hit you from a miles away.

This mod also includes zombies from the creature pack and so there are new zombies.
Lots of new zombies.

Zombies have soft heads so best to kill them with a head shot
    This also means that their bodies are capable of taking more damage. 

There is a new faction of zombie called the controlled that a hidden enemy is controlling. 
Often times they are seen with helmets and other armour and some have weapons grafted to them.
More work needs to be done on this. 


Blocks:
There is a new end game block made from the arm bones of zombies called zombzyme blocks great to build an end game base out of. 
Phosphorus tubes made with radioactive flesh to create a nice glowing tube.
Big doors and portcullises for those grand castles.
A few other have been added for various atmospheric reason too


SPIKES from alpha 16.4
  I have brought back the ability to craft the spikes from alpha 16.
  I have also expanded it with the ability to craft quicker crappier versions of the spikes too. Just incase you are in a pinch for time.

Progression dont worry about it. 
Just play and you will get better at things.
SCIENCE! included
NOTE: Not all "things" hooked up - Not the vanilla progression system.
        Needs work on the icons and the localization.
	More items need to be added. Cybernetics and gene mods


Vehicles:
Quadra V-Tech is in the game too. Likely for now mostly a creative menu
I'll get this into the loot system




Other Modders I would like to thank are:
DBat, Ironblade, Brazmock/Azmo, GuppyCurr, Khaine, Keledon, Haidr Gna, Sphereii, Stopmy NZ, Xyth, Darkstar Dragon, DanCap0, DeathtoDust, Mumphy, Manux. 
Really most of the modders on the guppy server :)




I am going to be working on the formatting of the page too
https://guides.github.com/features/mastering-markdown/