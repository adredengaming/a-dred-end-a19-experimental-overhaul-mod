Key,UsedInMainMenu,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish

flagPole,,,,,Flag Pole,,,,,
flagPoleDesc,,,,,An iron pole to put a flag on,,,,,

flagCanvas,,,,,Flag Canvas,,,,,
flagCanvasDesc,,,,,A blank canvas to make into any other flag,,,,,

FlagLooted,,,,,Looted Flag,,,,,
FlagLootedDesc,,,,,A flag to help show whats been looted,,,,,

FlagSedar,,,,,Sedar Flag,,,,,
FlagSedarDesc,,,,,The Flag for a very long time supporter Mr Sedar.,,,,,

FlagDarkMiss,,,,,DarkMisses Flag,,,,,
FlagDarkMissDesc,,,,,DarkMisses flag,,,,,

FlagPheonix,,,,,Dan Pheonix's Flag,,,,,
FlagPheonixDesc,,,,,A flag for a long time supporter Dan Pheonix,,,,,

FlagCanada,,,,,Canada Flag,,,,,
FlagCanadaDesc,,,,,The Flag of the greatest nation still on Earth,,,,,

FlagJaWoodle,,,,,JaWoodle Flag,,,,,
FlagJaWoodleDesc,,,,,Ya Na, who was this guy before the war ,,,,,

FlagNerdlette,,,,,Nerdlette Flag,,,,,
FlagNerdletteDesc,,,,,The YouTuber Nerdlette's flag, ,,,,,

FlagMrBen,,,,,"Mr. Ben" Flag,,,,,
FlagMrBenDesc,,,,,The YouTuber Mr. Ben's flag, ,,,,,